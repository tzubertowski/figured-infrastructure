## About
Blog application built for Figured recruitment process using:
- Laravel 5.6
- MongoDB
- nginx
- Laravel Passport

## Installation
For the sake of both simplicity of setup and in order not to litter your host machine this project is using docker and docker-compose.

You will need:

- [Docker](https://www.docker.com/)
- [docker-compose](https://www.docker.com/)

#### Setup
Once the pre-requirements are met navigate to the main folder and clone the projects:
- clone this (infrastructure) repository and navigate to it
```
git clone git@bitbucket.org:tzubertowski/figured-infrastructure.git figured-infra
cd figured-infra
```
- clone figured blog authentication service
```
git clone git@bitbucket.org:tzubertowski/figured-auth-service.git auth-service
``` 
- clone figured blog API
```
git clone git clone git@bitbucket.org:tzubertowski/figured-blog-service.git blog-service
```
- clone figured blog frontend
```
git clone git@bitbucket.org:tzubertowski/figured-blog-frontend.git blog-frontend
```
- Run
```
docker-compose up
```
- When the process succeeds add the vhosts to /etc/hosts
```
127.0.0.1 api.blog.dev.com
127.0.0.1 auth.blog.dev.com
127.0.0.1 fe.blog.dev.com
```

## Documentation & others
#### Accessing the app
Navigate to [figured-blog frontend page on your machine](http://blog.dev.com).

Admin credentials : admin/123qwe

#### API docs
- You can find API documentation in the figured blog api repository [swagger](https://bitbucket.org/tzubertowski/figured-blog-service/src/master/api.yml) file.
- Authentication service implements standard Laravel Password API routes

#### Running tests
Tests are implemented on blog-service with codeception. Connect to the container and run codeception
```
docker exec -it app-blog-api /bin/bash
./vendor/bin/codecept run
```


#### Notes
Due to the limited time I could spend on the test there are a few of improvements I'd do:
- *Use Doctrine ODM over Eloquent extension*. As there are no plans to take this application further it was easier to implement ODM extension for eloquent than Doctrine. Doctrine however could prove to be desired if need for advanced mongodb features would araise (eg. aggregation framework).

- *Introduce JSONtype driven responses*. APIs responses should be a definition of contract between clients and the provider. [JSONType](https://www.w3schools.com/js/js_json_datatypes.asp) is my get-go for response unification.

- NOTE: I didn't introduce any "extended" functionality over the basic CRUD as there was no requirement for it

- align docker setup to industry standards. I only included a basic one to ease up the project setup.

- introduce behat on the API project. I saw no reason to go into it now as the functional requirements were very basic and there's no plan to support this APP long term.

- as I mainly deal with backend of the projects in my day to day work I'd have to spend more time and rewrite the vue.js frontend. But on the other hand it was really fun to learn Vue :) Also - if I had time I'd test it.

- would there be need to extend authorization this solution comes packaged with Oauth 2 compatible REST microservice. All we need to do to implement, say, scopes would be to extend AuthorizationInterface